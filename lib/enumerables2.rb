require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
return 0 if arr == []
arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? {|string| sub_string?(substring,string)}
end

def sub_string?(sub_string,string)
  string.include?(sub_string)
end
# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  letters = string.delete(" ")
  letters.chars.uniq.reduce([]) do |arr,letter|
    if string.count(letter) >= 2
      arr.push(letter)
    else arr
    end
  end
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.delete("!,.:;?")
  words = string.split(" ")
  words.sort_by {|word| word.length}[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  letters = "abcdefghijklmnopqrstuvwxyz"
  letters.chars.reduce([]) do |arr,letter|
    if !string.include?(letter)
      arr.push(letter)
    else arr
    end
  end
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  all_years = (first_yr..last_yr).to_a
  all_years.reduce([]) do |arr,year|
    if not_repeat_year?(year)
      arr.push(year)
    else arr
    end
  end
end

def not_repeat_year?(year)
  year_arr = year.to_s.split("")
  year_arr = year_arr.map{|digit| digit.to_i}
  year_arr == year_arr.uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  wonders = songs.select { |song| no_repeats?(song,songs)}
  wonders.uniq
end

def no_repeats?(song_name, songs)
  songs.each.with_index do |song,idx|
    if song == song_name
      return false if songs[idx+1]==song_name
    end
  end
  return true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  words=remove_punctuation(string)
  words=words.split
  cwords = words.select {|word| word.downcase.include?("c")}
  return "" if cwords.empty?
  cwords.sort_by {|word| c_distance(word)}.first
end

def c_distance(word)
  revword = word.split("").reverse
  revword.index("c")
end

def remove_punctuation(string)
   string.delete("!,.;:?")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  multiples=[]
  subarray=[]
  counter = 0
  while counter < arr.length-1
    if arr[counter]==arr[counter+1]
      subarray.push(counter,counter+1)
      counter+=1
      while arr[counter]==arr[counter+1]
        counter+=1
        subarray.push(counter)
      end
      multiples << [subarray[0],subarray[-1]]
      subarray=[]
    else counter+=1
    end
  end
  multiples
end
